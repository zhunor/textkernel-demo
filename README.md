This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Approach 

- I select bulma.io as css framework for this demo because it is based on flex and easy to use.

- I select mapbox gl as map library because interactivity is flawless and modern.

- I've implemented a container component 'Jobfeed' for data fetching. This component also responsible for passing data to children 'Map' and 'Jobs'. 

- Jobs component is listing in jobs in cards. Map components are listing jobs as markers in a map. Both are receiving data from Jobfeed container component. 

- When something is deleted or filtered both components should be updated. these kind of interaction between Map and Jobs is maitained via Jobfeed component. 

### Run 
 - download zip or git clone https://zhunor@bitbucket.org/zhunor/textkernel-demo.git  
 - npm i 
 - npm start


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.


import React, { Component } from 'react';
import './Jobfeed.css';

import Map from '../Map/Map';
import Jobs from '../Jobs/Jobs';


class Jobfeed extends Component {

  constructor() {
    super()
    this.state = {
      jobs: []
    }
    this.immutableJobs = [];
  }


  componentDidMount() {
    this.getRecentJobs().then(data => {
      this.setState({
        jobs: data
      })
      this.immutableJobs = Array.from(data);
    });
  }


  getRecentJobs = async () => {
    const response = await fetch('/data/info-recent-jobs');
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  flyToJob = (job) => {
    if (job.location_coordinates && job.location_coordinates.length) {
      this.mapComponent.flyToJob(job.location_coordinates);
    }
  }

  // remove marker is invoked from sibling job component, access map component to remove it also from the map
  removeMarker = (idx) => {
    this.mapComponent.removeMarker(idx);
    const { jobs } = this.state;
    jobs.splice(idx, 1);
    this.setState({ jobs });
  }

  filterJobs = (e) => {
    const searchParameter = e.target.value;
    const getFilteredJobsFrom = (jobsArray) => {
      const jobsFiltered = jobsArray.filter(job => {
        if (job.job_title && job.job_title.toLowerCase().indexOf(searchParameter.toLowerCase()) > -1) {
          return true;
        }
        if (job.organization_name && job.organization_name.toLowerCase().indexOf(searchParameter.toLowerCase()) > -1) {
          return true;
        }
        return false;
      })

      return jobsFiltered;
    }

    // If user deletes a key get result from backup array
    if (e.keyCode === 8) {
      this.setState({
        jobs: getFilteredJobsFrom(this.immutableJobs)
      })
    } else {
      const { jobs } = this.state;
      this.setState({
        jobs: getFilteredJobsFrom(jobs)
      })
    }
  }


  render() {
    const { jobs } = this.state;

    if (jobs.length) {
      return (
        <div>
          <React.Fragment>
            <div >
              <Map jobs={jobs} ref={el => { this.mapComponent = el }} />
            </div>
            <div className="side-menu">
              <Jobs jobs={jobs} flyToJob={this.flyToJob} removeMarker={this.removeMarker} filterJobs={this.filterJobs} />
            </div>
          </React.Fragment>
        </div>
      );
    }


    // Loading 
    return (
      <React.Fragment>
        <div className="center-children">
          <h1>LOADING...</h1>
        </div>
      </React.Fragment>
    )
  }
}

export default Jobfeed;
import React, { Component } from 'react';
import mapboxgl from 'mapbox-gl';
import mapMarker1 from '../../assets/alien.svg'
import mapMarker2 from '../../assets/man.svg'
import mapMarker3 from '../../assets/planetary.svg'


mapboxgl.accessToken = 'pk.eyJ1Ijoib3plcm9yaHVuIiwiYSI6ImNqYmF4NHh2dTEwbTAycHAzbnd4azhwcGEifQ.LsST6QrnJ0XEar6wgnnfSg';

class Map extends Component {

  constructor() {
    super()
    this.state = {
      jobs: [],
      markers: []
    }

    // map object to share
    this.map = {};
  }

  componentDidMount() {
    const { jobs } = this.props;
    this.setState({
      jobs
    })
    this.initMap(jobs);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ jobs: nextProps.jobs });
    // re-draw markers on filter change
    this.drawMarkers(nextProps.jobs);
  }

  initMap = (jobs) => {

    this.map = new mapboxgl.Map({
      container: this.mapContainer,
      style: 'mapbox://styles/mapbox/streets-v9',
      center: [-101.92426, 40.70048],
      zoom: 3,
      pitch: 50,
      bearing: 27
    });

    this.drawMarkers(jobs);

  }

  cleanMarkers = () => {
    let { markers } = this.state;
    markers.forEach(marker => marker.remove())
    markers = [];
  }

  drawMarkers = (jobs) => {

    this.cleanMarkers();
    const markers = [];

    const addMarker = (job, i) => {
      // create DOM element for the marker

      if (job.location_coordinates && job.location_coordinates.length) {
        let markerIcon;
        // swicth marker icons just for fun 
        // they might be usefull to visualize different job categories
        switch (i % 3) {
          case 0:
            markerIcon = mapMarker1;
            break;
          case 1:
            markerIcon = mapMarker2;
            break;
          default:
            markerIcon = mapMarker3;
            break;
        }

        const el = document.createElement('div');
        el.className = 'marker';
        el.style.background = `url(${markerIcon})`;
        el.style.width = '32px';
        el.style.height = '32px';
        el.style.backgroundSize = '32px 32px';
        el.style.backgroundRepeat = 'no-repeat';

        // create the popup
        const popup = new mapboxgl.Popup({ offset: 25 })
          .setText(`${job.job_title } @${ job.organization_name}`);

        try {
          const marker = new mapboxgl.Marker(el)
            .setLngLat([job.location_coordinates[1], job.location_coordinates[0]])
            .setPopup(popup)
            .addTo(this.map);
          markers.push(marker);
        } catch (error) {
          console.log(error);
        }

      }
    }

    jobs.forEach((job, i) => {
      addMarker(job, i);
    })

    this.setState({ markers })

  }

  flyToJob = (coords) => {
    this.map.flyTo({
      center: [coords[1], coords[0]],
      zoom: 11
    });
  }

  removeMarker = (idx) => {
    const { markers } = this.state;
    markers[idx].remove();
    markers.splice(idx, 1);
    this.setState({ markers });
  }


  render() {
    return (
        <div ref={el => { this.mapContainer = el }} className="map-container" />
    );
  }
}

export default Map;
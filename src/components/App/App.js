import React from 'react';
import './App.css';

import Navbar from '../Navbar/Navbar';
import Jobfeed from '../Jobfeed/Jobfeed';

const App = () => {

  return (
    <React.Fragment>
      <Navbar/>
      <Jobfeed/>
   
    </React.Fragment>
  );
}

export default App;

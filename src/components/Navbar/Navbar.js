import React from 'react';
import logo from '../../logo@2x.png';

const Navbar = () => {
  return (
    <nav className="navbar is-fixed-top" role="navigation" aria-label="main navigation">
      <div className="navbar-brand">
        <a className="navbar-item" href="/">
          <img src={logo} className="logo" alt="" />
          
                </a>

        <button type="button" className="navbar-burger" aria-label="menu" aria-expanded="false" href="#">
          <span aria-hidden="true" />
          <span aria-hidden="true" />
          <span aria-hidden="true" />
        </button>
      </div>
    </nav>
  )
}
export default Navbar;
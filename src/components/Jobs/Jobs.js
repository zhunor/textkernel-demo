import React, { Component } from 'react';
import { chunk } from '../../utils/common';

class Jobs extends Component {
  constructor() {
    super()
    this.state = {
      jobs: [],
    }
  }

  componentDidMount() {
    const { jobs } = this.props;
    this.setState({
      jobs
    })
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ jobs: nextProps.jobs });
  }


  renderJobs = () => {
    const { jobs } = this.state;
    const { flyToJob, removeMarker, filterJobs } = this.props;
    // i am dividing data into chunks to visualize it in flex-columns which fill 50% of the parent div
    const rows = chunk(jobs, 2);

    return (
      <div>
        <h1 className="title is-1 white">Recent Jobs</h1>
        <div className="field">
          <input className="input" type="text"
            placeholder="Search for company or job"
            onKeyUp={filterJobs}></input>
        </div>
        {
          rows.map((row, rowIndex) =>
            <div className="columns">
              {
                row.map((job, i) => (
                  <div className="column is-half" key={job.job_title}>
                    <div className="card card-reverse">
                      <p className="has-text-right">
                        {/* This might seems weird but in order to send correct index, previous chunks ids' also added */}
                        <button type="button" className="delete" onClick={() => removeMarker(i + (rowIndex * 2))}></button>
                      </p>
                      <div className="card-content">
                        <div className="media">
                          <div className="media-content">
                            <p className="title is-5">{job.job_title}</p>
                            <p className="subtitle is-6">@{job.organization_name}   </p>
                          </div>
                        </div>

                        <div className="content">
                          <p className="has-text-right">
                            <button type="button" className="button is-primary is-outlined is-fullwidth"
                              onClick={() => flyToJob(job)}> Show me </button>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                ))
              }
            </div>
          )
        }
      </div>
    );
  }

  render() {
    return this.renderJobs();
  }
}

export default Jobs;